import { Injectable } from '@angular/core';
import { Country } from './country.model';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  countriesChange = new Subject<Country[]>();
  countriesLoading = new Subject<boolean>();

  countries: Country[] = [];

  constructor(private http: HttpClient) {}

  getCountries() {
    return this.countries.slice();
  }

  fetchCountries() {
    this.countriesLoading.next(true);
    this.http.get<{[id:string]:Country}>('http://146.185.154.90:8080/restcountries/rest/v2/all?fields=name%3Balpha3Code')
      .pipe(map( result => {
        if(result === null) return [];
        return Object.keys(result).map(id => {
          const country = result[id];
          return new Country(country.alpha3Code, country.name);
        });
      }))
      .subscribe((countries:Country[]) => {
        this.countries = countries;
        this.countriesChange.next(this.countries.slice());
        this.countriesLoading.next(false);
      }, error => {
        this.countriesLoading.next(false);
        console.log(new Error(error.message));
      });
  }

  fetchCountry(code: string) {
    return this.http.get<Country>('http://146.185.154.90:8080/restcountries/rest/v2/alpha/'+code)
      .pipe(map(result => {
        return new Country(
          code,
          result.name,
          result.capital,
          result.flag,
          result.population,
          result.languages,
          result.region,
          result.subregion,
          result.area,
          );
      })
    );
  }
}
