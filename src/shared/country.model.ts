type languagesType = {
  name:string
}
export class Country {
  constructor(
    public alpha3Code:string,
    public name:string,
    public capital = '',
    public flag = '',
    public population = 0,
    public languages:languagesType[] = [],
    public region = '',
    public subregion = '',
    public area = 0,
  ) {}
}
