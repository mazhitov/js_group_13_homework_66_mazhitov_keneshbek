import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Country } from './country.model';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryResolverService implements Resolve<Country> {

  constructor(private httpService:HttpService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Country>{
    const code = <string>route.params['code'];
    return this.httpService.fetchCountry(code);
  }
}
