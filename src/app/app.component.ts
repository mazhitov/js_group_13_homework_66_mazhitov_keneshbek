import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpService } from '../shared/http.service';
import { Country } from '../shared/country.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  countries: Country[] = [];
  isLoading = false;
  countriesLoadingSubscription!: Subscription;
  countriesChangeSubscription!: Subscription;

  constructor(private httpService: HttpService) {
  }

  ngOnInit() {
    this.httpService.getCountries();

    this.countriesChangeSubscription = this.httpService.countriesChange.subscribe((countries: Country[]) => {
      this.countries = countries;
    });
    this.countriesLoadingSubscription = this.httpService.countriesLoading.subscribe((isLoading: boolean) => {
      this.isLoading = isLoading;
    });
    this.httpService.fetchCountries();
  }

  ngOnDestroy() {
    this.countriesChangeSubscription.unsubscribe();
    this.countriesLoadingSubscription.unsubscribe();
  }
}
