import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Country } from '../../shared/country.model';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.css']
})
export class CountryDetailsComponent implements OnInit {
  country: Country | null = null;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.country = <Country>data.country;
    });
  }

  getImgUrl() {
    return `http://146.185.154.90:8080/restcountries/data/${this.country?.alpha3Code.toLowerCase()}.svg`;
  }
}
